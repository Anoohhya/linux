-> % of marks as 'arg'
# script_name <value>
    $0          $1

-> Logical test/expression:
1.) AND (-a)
2.) OR (-o)
3.) NOT (!)

- # vim student.sh
-------============------------=========
#!/bin/bash
# student result : percentage of marks
# define initial percentage of student in a variable

PERC =$1

# to check whether we are passing perc value as arg or not
if [ ! $PERC ]; then
    echo "please pass student percentage as argument: MANDATORY..!!"
    exit
fi

# to restrict student perc between 0 and 100
# using or (-o) logical operator
if [ "$PERC" -gt 100 -o "$PERC" -lt 0 ]; then  
    echo "please enter student percentage between 0 and 100 only...!!!"
    exit
fi

if [ "$PERC" -gt 65 ]; then
    echo "Student result: First division"
elif [ "$PERC" -gt 55 ]; then
    echo "Student result: Second Division"
elif [ "$PERC" -ge 45 ]; then
    echo "Student result: third division"
else 
    echo "Student result : failed
fi
---=================------------==========------------=======

--------------------------------------------------------------------
Scenario: write a shell script to create a user in linux and set    |
          the password fro newly created user?                      |
1.) Take newly user name as Command line argument:                  |
2.) Executing/running user sufficient previleges to add/create      |
3.) Check if the user already existing or not                       |
        if 'yes' -> print the statement                             |
        if 'no'  -> create the user                                 |
4.) set the password for the user                                   |
5.) successfully created.                                           |
---------------------------------------------------------------------

----======---vim createuser.sh---===-=-=-=
#!/bin/bash

# create user and set the password if user is not existing already
MYUSER = $1
MYPASS='default123'

if [ !$MYUSER ]; then
    echo "please enter username to add : MANDATORY...!!"
    exit
fi

RUNUSER=`/usr/bin/whoami`
if [ "$RUNUSER" != 'root' ]; then
    echo "You must be a root user to create a newone..."
    exit
fi

/usr/bin/id $MYUSER
if [ $? -eq 0 ]; then 
    echo "The user you are trying to create is already existing...
    please try a different name"
    exit
fi
/sbin/useradd $MYUSER
echo "$MYPASS" | /usr/bin/passwd --stdin $MYUSER

echo "Congratulations... The user $MYUSER created successfully...!!"

------==============---------=====-------------============-=

* $? -> this is used to check whether previous Command execution is successfull or not
--> if successfull then bash returns '0'
--> if not successfull then bash returns '1' 

sbin -> superuser bin



