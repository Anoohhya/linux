Functions:
--> Block of code which is used multiple times by calling the function.
=>function is a name/ label
? will function accept arguments -> not => will return anything.

variable:
--> name / label given to memory location, which is used to call that variable where ever needed.

------------
How to define the syntax of the function?

-->function_name() {
    --------
    -------   #(body of the function)
    --------
    --------
}

------------
How to write function file?

1.) function file does not have .sh extention (it is not a shellscripting)
2.) function does not starts with shabang line (#!/bin/bash)
3.) function file does not have execute permissions.

-----------
How to use/ run a function?

func_file:
---
---
---
---

-> we need to execute this function file as a linux. How?
--> we have/ need to load function file into the memory of the O/S

example func:

func_file
-=-=-=-=-=-=-
goodmorning() {
    echo "hello goodmorning"
    echo "today your tasks are..."
}
-=-=-=-=-=-=-

to load function file to the memory

#source <function file name>

#source func_file

Run function:

# goodmorning
  ---
  ---
  ---
---------------------------------------------
In instance

# vim funcfile
============================
#no need of .sh extension
#no need of shabang line
#no need of +x permissions

goodmorning() {

    echo "Welcome to shellscripting...!!"
    echo "A very Good morning to you..!!"
    echo "Todays date is :"
    date

}
===================================

# source funcfile

# goodmorning
---------------------------------
# vim funcfile
============================
#no need of .sh extension
#no need of shabang line
#no need of +x permissions

goodmorning() {

    echo "Welcome to shellscripting...!!"
    echo "A very Good morning to you..!!"
    echo "Todays date is :"
    date

}

takingbkp() {
    echo "taking backup of $1 directory"
    sleep 15
    echo "backup successfull..!!"

}
==========================================
--> each time after updating the function file we need to source the file.

# takingbkp /tmp
-----------------------------------------------------------------------

# funcscript.sh
=======------------=========-----------==========
#!/bin/bash
# defining and calling functions from the shell script
# short path method

. funcfile

# full path method

. /home/ec2-user/scripts/funcfile

# call functions in function file
goodmorning

echo

takingbkp
=====----==========---------============

# funcscript.sh
=======------------=========-----------==========
#!/bin/bash
# defining and calling functions from the shell script
# short path method

. funcfile

# full path method

. /home/ec2-user/scripts/funcfile

# call functions in function file
goodmorning

echo

takingbkp

echo

# goodbye

goodbye() {
    echo "Thats it from the shellscripting...!!"
    echo "Please explore more on these topics"
    echo "Good bye... and see you tomorrow"
}

goodbye

## will give the error because we calling the goodbye function before creating the goodbye function
=====----==========---------============

How to remove the functionality from the source of the function file

# unset -f funcfile

--> logout and login back

# goodmorning 
